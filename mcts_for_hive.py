from __future__ import division
import numpy as np
import random as rnd
from piece import HivePiece
from board import Board
import copy
import pickle
import time
import math
import random

class treeNode():
    def __init__(self, state, parent):
        self.state = state
        self.isTerminal = state.isTerminal()
        self.isFullyExpanded = self.isTerminal
        self.parent = parent
        self.numVisits = 0.1
        self.totalReward = 0
        self.Reward=0
        self.children = {}
        self.cause_move=None

class Player:
    def __init__(self):
        pass
        
    
    def start(self, player):
        self.player = player
           
    def next_move(self, hive):
        
        self.monte_carlo= mcts(timeLimit=4000)
        bestmove=self.monte_carlo.search(hive)
        return bestmove
class mcts():
    def __init__(self, timeLimit=None, iterationLimit=None, explorationConstant=1 / math.sqrt(2)):
        try:
            self.transposition_table = pickle.load( open( "transpositionTableUcbMcts.p", "rb" ) )
        except:
            self.transposition_table={}
            
        if timeLimit != None:
            if iterationLimit != None:
                raise ValueError("Cannot have both a time limit and an iteration limit")
            # time taken for each MCTS search in milliseconds
            self.timeLimit = timeLimit
            self.limitType = 'time'
        else:
            if iterationLimit == None:
                raise ValueError("Must have either a time limit or an iteration limit")
            # number of iterations of the search
            if iterationLimit < 1:
                raise ValueError("Iteration limit must be greater than one")
            self.searchLimit = iterationLimit
            self.limitType = 'iterations'
        self.explorationConstant = explorationConstant
        self.epsilon=0.01
    def search(self, initialState):
        self.root = treeNode(initialState, None)

        if self.limitType == 'time':
            timeLimit = time.time() + self.timeLimit / 1000
            while time.time() < timeLimit:

                c=self.executeRound()
                
                if c != None:
                    a,b=c
                    if a=='terminal':
                        return self.getAction(self.root,b)
               
        else:
            for i in range(self.searchLimit):
                c=self.executeRound()
                if c != None:
                    a,b=c
                    if a =='terminal':
                        return self.getAction(self.root,b)
                
        bestChild = self.getBestChild(self.root, 0)
        return self.getAction(self.root, bestChild)
    
    def rollout(self,nodstate):
        
        reward=0
        state=copy.deepcopy(nodstate)
        while state.isTerminal()==False:
                         
            possMove=state.possible_moves()
            if len(possMove)>=1:
                move=rnd.choice(possMove)
                state.action('play',move)
                state.rollout_turn+=1

            else:
                state.turn += 1
                state.activePlayer ^= 1  
                state.rollout_turn+=1

        if nodstate.activePlayer==state.activePlayer:
            if state.check_victory()==state.DRAW:
                reward= 0.5
            else:
                reward= 1
        else:
            reward= 0
                    
        return reward
    

    def executeRound(self):
        node = self.selectNode(self.root)
        if node.isTerminal:
            return 'terminal',node
        node_key= self.hash_key(node)
            
        if node_key in self.transposition_table.keys():
            if random.random()>self.epsilon:
                reward,visit=self.transposition_table[node_key]
                
                
            else:
                reward = self.rollout(node.state)
                visit=node.numVisits
        else:
            reward = self.rollout(node.state)
            visit=node.numVisits+1
            rew=node.Reward+reward
            self.transposition_table[self.hash_key(node)]=(rew,visit)
            
        self.backpropogate(node, reward,visit)
        

    def selectNode(self, node):
        while not node.isTerminal:
            if node.isFullyExpanded:
   
                bestChildnode = self.getBestChild(node, self.explorationConstant)
            else:
      
                return self.expand(node)
         
        return bestChildnode

    def expand(self, node):
        moves= node.state.possible_moves()
        actions=self.remove(moves)
        terminalNodes=[]
        for move in actions:

            if str(move) not in node.children.keys():
                anode=copy.deepcopy(node)
                aaa=anode.state.action('play',move)
                newNode = treeNode(anode.state, node)
                newNode.cause_move=move
                if newNode.isTerminal:
                    check=newNode.state.check_victory()
                    if check==newNode.state.BLACK_WIN:
                        if node.state.activePlayer==1:
                            terminalNodes.append(newNode)
                    elif check==newNode.state.WHITE_WIN:
                        if node.state.activePlayer==0:
                            terminalNodes.append(newNode)
                                           
                                        
                node.children[str(move)] = newNode
                if len(actions) == len(node.children):
                    node.isFullyExpanded = True
                    if len(terminalNodes)>0:
                        return random.choice(terminalNodes)
                            
                    else:
                        return newNode
            else:
                raise Exception("Should never reach here")

    def remove(self,duplicate=[(0,0)]):
        """
        Returns the list after removing duplicate elements from the list(duplicate)
        """
        final_list = [] 
        for num in duplicate:
     
            if num not in final_list: 
                final_list.append(num) 
        return final_list 
    
    def backpropogate(self, node, reward,visit):
        
        while node is not None:
            node.numVisits += visit
            node.Reward += reward
            node.totalReward=node.Reward / node.numVisits
        
            node = node.parent
            
    def getBestChild(self, node, explorationValue):
        bestValue = float("-inf")
        bestNodes = []
        for child in node.children.values():
       
            nodeValue = child.totalReward + explorationValue * math.sqrt(2 * math.log(node.numVisits) / child.numVisits)
   
            if nodeValue > bestValue:
                bestValue = nodeValue
                bestNodes = [child]
            elif nodeValue == bestValue:
                bestNodes.append(child)
        return random.choice(bestNodes)

    def getAction(self, root, bestChild):
        move=[]
        for action, node in root.children.items():
            if node is bestChild:
                
                move=node.cause_move
        
                if bestChild.isTerminal:
                    pickle.dump( self.transposition_table, open( "transpositionTableUcbMcts.p", "wb" ) )
                return move
            
            
    def hash_key(self,node):
        
        
        pp=[]
        temp=[]
        for k,v in node.state.playedPieces.items():
            temp.append(v)
            
        for c in temp:
            pp.append((c['piece'],c['cell']))
        
        pp.sort(key=lambda x:str(x[0]))

        key=0
        for (x,y) in pp:
            xx,yy=y
            if xx<0:
                #print(xx,"xx")
                key=key+(hash(x)+(-23*hash(xx))+hash(yy))
            else:
                key=key+(hash(x)+(23*hash(xx)+hash(yy)))
               
        if node.state.activePlayer==0:
            player_key=key*2
        else:
            player_key=key*2+1
        return player_key
        

        
    
