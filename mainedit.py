#! /usr/bin/env python
import copy
import time
import sys
import random as rnd
import numpy as np
from board import HexBoard
from hive import Hive, HiveException
from piece import HivePiece
from view import HiveView


class HiveShellClient(object):
    """HiveShellClient is a command line client to the Hive game."""

    def __init__(self):
        super().__init__()
      



    def run(self ,hive,  player1,player2):
        
        self.view = HiveView(hive)
        
        pl = [player1, player2] # A list to make it easier to alternate between two players.
        
       
     
        hive.setup()
        pl[hive.activePlayer].start(1)
        pl[hive.activePlayer+1].start(2)
        
        while hive.check_victory() == hive.UNFINISHED:
            active_player = (2 - (hive.turn % 2))
            print("Turn: %s" % hive.turn)
            print(self.view)


            print("player %s play: " % active_player, end=' ')
            try:
               
                cmd = pl[hive.activePlayer].next_move(copy.deepcopy(hive))
           
            except KeyboardInterrupt as e:
                break
            if cmd==None:
                 hive.turn += 1
                 hive.activePlayer ^= 1 
            elif cmd!=None:
                if hive.action('play', cmd):
                    print()
                    print("=" * 79)
                    print()

            else:
                print("invalid play!")
        print(self.view)
        print("Player : %s wins" % (2- ((hive.turn-1)%2)),end=' ' )
        print("\nThanks for playing Hive. Have a nice day!")


if __name__ == '__main__':
    import sys, ast
    # For the time being, assume that there are only two players and that we are given the modules of both.
    code = ast.parse(open(sys.argv[1]).read())
    eval(compile(code, '', 'exec'))
    pl1 = Player()
   

    code = ast.parse(open(sys.argv[2]).read())
    eval(compile(code, '', 'exec'))
    pl2 = Player()
    
    game = HiveShellClient()
    hive=Hive()
    game.run(hive,pl1,pl2)


