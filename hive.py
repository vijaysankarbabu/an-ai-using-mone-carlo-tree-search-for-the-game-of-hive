from board import HexBoard
from piece import HivePiece


class HiveException(Exception):
    """Base class for exceptions."""
    pass

class Hive(object):
    """
    The Hive Game.
    This class enforces the game rules and keeps the state of the game.
    """

    # Directions
    O = HexBoard.HX_O    # origin/on-top
    W = HexBoard.HX_W    # west
    NW = HexBoard.HX_NW  # north-west
    NE = HexBoard.HX_NE  # north-east
    E = HexBoard.HX_E    # east
    SE = HexBoard.HX_SE  # south-east
    SW = HexBoard.HX_SW  # south-west

    # End game status
    UNFINISHED = 0
    WHITE_WIN = 1
    BLACK_WIN = 2
    DRAW = 3


    def __init__(self):
        self.turn = 0
        self.activePlayer = 0
        self.players = ['w', 'b']
        self.board = HexBoard()
        self.playedPieces = {str('wG3'):{'piece':HivePiece('w','G','3'),'cell':(-2,-2)},str('bG3'):{'piece':HivePiece('b','G','3'),'cell':(-3,2)}, str('bB2'):{'piece':HivePiece('b','B','2'),'cell':(-2,-1)},str('bG2'):{'piece':HivePiece('b','G','2'),'cell':(-2,0)},str('wB1'):{'piece':HivePiece('w','B','1'),'cell':(-1,0)},str('wG2'):{'piece':HivePiece('w','G','2'),'cell':(-3,1)},str('wG1'):{'piece':HivePiece('w','G','1'),'cell':(-2,1)},str('wA3'):{'piece':HivePiece('w','A','3'),'cell':(-1,1)},str('bQ1'):{'piece':HivePiece('b','Q','1'),'cell':(0,0)},str('bA1'):{'piece':HivePiece('b','A','1'),'cell':(-2,2)},str('bB1'):{'piece':HivePiece('b','B','1'),'cell':(-1,2)},str('wS2'):{'piece':HivePiece('w','S','2'),'cell':(2,-2)},str('wQ1'):{'piece':HivePiece('w','Q','1'),'cell':(1,-1)},str('wA1'):{'piece':HivePiece('w','A','1'),'cell':(1,0)},str('bG1'):{'piece':HivePiece('b','G','1'),'cell':(0,1)},str('bS2'):{'piece':HivePiece('b','S','2'),'cell':(0,2)},str('bA3'):{'piece':HivePiece('b','A','3'),'cell':(3,-2)},str('bA2'):{'piece':HivePiece('b','A','2'),'cell':(2,-1)},str('bS1'):{'piece':HivePiece('b','S','1'),'cell':(2,0)},str('wS1'):{'piece':HivePiece('w','S','1'),'cell':(1,1)},str('wB2'):{'piece':HivePiece('w','B','2'),'cell':(1,2)},str('wA2'):{'piece':HivePiece('w','A','2'),'cell':(2,1)}}
        self.piecesInCell = {(-2,-2):[str(HivePiece('w','G','3'))],(-3,2):[str(HivePiece('b','G','3'))],(-2,-1):[str(HivePiece('b','B','2'))],(-2,0):[str(HivePiece('b','G','2'))],(-1,0):[str(HivePiece('w','B','1'))],(-3,1):[str(HivePiece('w','G','2'))],(-2,1):[str(HivePiece('w','G','1'))],(-1,1):[str(HivePiece('w','A','3'))],(0,0):[str(HivePiece('b','Q','1'))],(-2,2):[str(HivePiece('b','A','1'))],(-1,2):[str(HivePiece('b','B','1'))],(2,-2):[str(HivePiece('w','S','2'))],(1,-1):[str(HivePiece('w','Q','1'))],(1,0):[str(HivePiece('w','A','1'))],(0,1):[str(HivePiece('b','G','1'))],(0,2):[str(HivePiece('b','S','2'))],(3,-2):[str(HivePiece('b','A','3'))],(2,-1):[str(HivePiece('b','A','2'))],(2,0):[str(HivePiece('b','S','1'))],(1,1):[str(HivePiece('w','S','1'))],(1,2):[str(HivePiece('w','B','2'))],(2,1):[str(HivePiece('w','A','2'))]}
        self.unplayedPieces = {}
        self.unoccupiedCells = []
        self.player_just_moved =2
        self.rollout_turn=0
      #  self.logger = None
      
      

    
    def setup(self):
        """
        Prepare the game to be played
        """
        # Add pieces to the players hands
        self.unplayedPieces['w'] = self._piece_set('w')
        self.unplayedPieces['b'] = self._piece_set('b')
        self.turn = 9

   
    
    def action(self, actionType, action):

        (startCell,endCell)=action
        start_cell=startCell 
        Munocc=[]

        """Perform the player action.
        return True or ExceptionType
        TODO: elaborate on the exceptions
        """
        if (actionType == 'play'):
            if (isinstance(action, tuple)):
                (starCell,endCell) = action
            elif (isinstance(action, str)):
                (startCell,endCell) = (action, None, None)
            player = self.get_active_player()
            if str(start_cell) in self.unplayedPieces[player]:
                actPiece=start_cell
                
                piece = self.unplayedPieces[player].get(str(actPiece), None)
                if piece is not None:
                    self.place_piece(piece, endCell)
                    PtargetCell=endCell

                    sur=self._unoccupied_surroundings(PtargetCell)
                    self.unoccupiedCells.extend(sur)
                    Punocc= self.unoccupiedCells
                    for c in Punocc:
                        if c== PtargetCell:
                            Punocc.remove(PtargetCell)

                    self.unoccupiedCells= self.remove(Punocc)
                    del self.unplayedPieces[player][str(actPiece)]

            else:
                Mact=self._unoccupied_surroundings(start_cell)
                temp=[]
                Munocc=self.unoccupiedCells
                aPc=self.get_pieces(start_cell)
                aPiece=str(aPc)

                if len(aPiece) == 7:
                    player = aPiece[2]
                    pc = aPiece[3]
                    num = aPiece[4]
                    actPiece=HivePiece( player,pc,num)
                       
                    ppiece = self.playedPieces.get(str(actPiece), None)
                    if ppiece is None:
                        raise HiveException
                    else:
                        self.move_piece(ppiece['piece'], endCell)
                        for c in Mact:
                            
                            b=self._unoccupied_surroundings(c)
                
                            if len(b)==6:
                                temp.append(c)

                                
                        final=[]
                        MtargetCell=endCell
                        Msur= self._unoccupied_surroundings(MtargetCell)
                        Munocc.extend(Msur)
                  
                        Munocc.append(start_cell)
                        for c in Munocc:
                            if c== MtargetCell:
                                Munocc.remove(MtargetCell)
                        final=list(set(Munocc).difference(set(temp)))

                        self.unoccupiedCells = final
               

                    self.unoccupiedCells=self.remove(final)
                    


        elif (actionType == 'non_play' and action == 'pass'):
            pass

        # perform turn increment - TODO:if succesful
        self.turn += 1
        self.activePlayer ^= 1  # switch active player
        self.player_just_moved = 3 - self.player_just_moved
        
        if self.turn>=9:
            ss=self.alignBoard()
        
        
        return True


    def get_unplayed_pieces(self, player):
        return self.unplayedPieces[player]


    def get_active_player(self):
        if self.turn <= 0:
            return None

        return self.players[self.activePlayer]

    def get_board_boundaries(self):
        """returns the coordinates of the board limits."""
        return self.board.get_boundaries()


    def get_pieces(self, cell):
        """return the pieces that are in the cell (x, y)."""
        return self.piecesInCell.get(cell, [])


    def locate(self, pieceName):
        """
        Returns the cell where the piece is positioned.
        pieceName is a piece identifier (string)
        """
        res = None
        pp = self.playedPieces.get(pieceName)
        if pp is not None:
            res = pp['cell']


        return res
    
    def remove(self,duplicate=[(0,0)]):
        """
        Returns the list after removing duplicate elements from the list(duplicate)
        """
        final_list = [] 
        for num in duplicate:
     
            if num not in final_list: 
                final_list.append(num) 
        return final_list 


    def move_piece(self, piece,endCell):
        """
        Moves a piece on the playing board.
        """

        pieceName = str(piece)
        targetCell = endCell

        # is the move valid
        if not self._validate_turn(piece, 'move'):
            raise HiveException("Invalid Piece Placement")

        if not self._validate_move_piece(piece, targetCell):
            raise HiveException("Invalid Piece Movement")

        pp = self.playedPieces[pieceName]
        startingCell = pp['cell']

        # remove the piece from its current location
        self.piecesInCell[startingCell].remove(pieceName)

        # places the piece at the target location
        self.board.resize(targetCell)
        pp['cell'] = targetCell
        pic = self.piecesInCell.setdefault(targetCell, [])
        pic.append(str(piece))

        return targetCell


    def place_piece(self, piece,  endCell):
        """
        Place a piece on the playing board.
        """
        # if it's the first piece we put it at cell (0, 0)
        if self.turn == 1:
            targetCell = (0, 0)
       
        else:
            targetCell = endCell

        # is the placement valid
        if not self._validate_turn(piece, 'place'):
            raise HiveException("Invalid Piece Placement")

        if not self._validate_place_piece(piece, targetCell):
            raise HiveException("Invalid Piece Placement")

        # places the piece at the target location
        self.board.resize(targetCell)
        
        self.playedPieces[str(piece)] = {'piece': piece, 'cell': targetCell}
        """
        this is the actual code.. i have changed it to above code so that i get only the cell of played piece..
               self.playedPieces[str(piece)] = {'piece': piece, 'cell': targetCell}
        """
        pic = self.piecesInCell.setdefault(targetCell, [])
        pic.append(str(piece))
        ss= self.board.get_boundaries()

        return targetCell
    
    def isTerminal(self):
        if self.check_victory()!= self.UNFINISHED:
            return True
        else:
            return False
        
               
  
    def check_victory(self):
        """
        Check if white wins or black wins or draw or not finished
        """
        white = False
        black = False
        res = self.UNFINISHED

        # if white queen is surrounded => black wins
        queen = self.playedPieces.get('wQ1')
        if(queen is not None and len(self._occupied_surroundings(queen['cell'])) == 6):
            black = True
            res = self.BLACK_WIN

        # if black queen is surrounded => white wins
        queen = self.playedPieces.get('bQ1')
        if(queen is not None and len(self._occupied_surroundings(queen['cell'])) == 6):
            white = True
            res = self.WHITE_WIN

        # if both queens are surrounded
        if white and black:
            res = self.DRAW

        return res
    
   
    def _validate_turn(self, piece, action):
        """
        Verifies if the action is valid on this turn.
        """
        whiteTurn = self.activePlayer == 0
        blackTurn = self.activePlayer == 1
        if whiteTurn and piece.color != 'w':
            return False

        if blackTurn and piece.color != 'b':
            return False

        # Tournament rule: no queen in the first move
        if (self.turn == 1 or self.turn == 2) and piece.kind == 'Q':
            return False

        # Move actions are only allowed after the queen is on the board
        if action == 'move':
            if blackTurn and ('bQ1' not in self.playedPieces):
                return False
            if whiteTurn and ('wQ1' not in self.playedPieces):
                return False

        # White Queen must be placed by turn 7 (4th white action)
        if self.turn == 7:
            if 'wQ1' not in self.playedPieces:
                if str(piece) != 'wQ1' or action != 'place':
                    return False

        # Black Queen must be placed by turn 8 (4th black action)
        if self.turn == 8:
            if 'bQ1' not in self.playedPieces:
                if str(piece) != 'bQ1' or action != 'place':
                    return False
        return True


    def _validate_move_piece(self, moving_piece, targetCell):
        # check if the piece has been placed

        pp = self.playedPieces.get(str(moving_piece))
        if pp is None:
            print("piece was not played yet")
            return False

        # check if the move it's to a different targetCell
        if str(moving_piece) in self.piecesInCell.get(targetCell, []):
            print("moving to the same place")
            return False

        # check if moving this piece won't break the hive
        if not self._one_hive(moving_piece):
            #print("break _one_hive rule")
            return False

        validate_fun_map = {
            'A': self._valid_ant_move,
            'B': self._valid_beetle_move,
            'G': self._valid_grasshopper_move,
            'Q': self._valid_queen_move,
            'S': self._valid_spider_move
        }
   
        return validate_fun_map[moving_piece.kind](pp['piece'], pp['cell'], targetCell)


    def _validate_place_piece(self, piece, targetCell):
        """
        Verifies if a piece can be played from hand into a given targetCell.
        The piece must be placed touching at least one piece of the same color
        and can only be touching pieces of the same color.
        """

        # targetCell must be free
     
        if not self._is_cell_free(targetCell):
            return False

        # the piece was already played
        if str(piece) in self.playedPieces:
            return False

        # if it's the first turn we don't need to validate
        if self.turn == 1:
            return True

        # if it's the second turn we put it without validating touching colors
        if self.turn == 2:
            return True

        playedColor = piece.color

        occupiedCells = self._occupied_surroundings(targetCell)
        visiblePieces = [self.piecesInCell[oCell][-1] for oCell in occupiedCells]
        res = True
        for pName in visiblePieces:
            if self.playedPieces[pName]['piece'].color != playedColor:
                res = False
                break
        return res


    def _is_cell_free(self, cell):
        pic = self.piecesInCell.get(cell, [])
        return len(pic) == 0


    def _occupied_surroundings(self, cell):
        """
        Returns a list of surrounding cells that contain a piece.
        """
        surroundings = self.board.get_surrounding(cell)
        return [c for c in surroundings if not self._is_cell_free(c)]
    
    
    def _unoccupied_surroundings(self, cell):
        """
        Returns a list of surrounding cells that contain a piece.
        """
        surroundings = self.board.get_surrounding(cell)
        unocc= [c for c in surroundings if self._is_cell_free(c)]
        return unocc
    # TODO: rename/remove this function.
    def _poc2cell(self, refPiece, pointOfContact):
        
        """
        Translates a relative position (piece, point of contact) into
        a board cell (x, y).
        """
        refCell = self.locate(refPiece)
        return self.board.get_dir_cell(refCell, pointOfContact)


    def _bee_moves(self, cell):
        """
        Get possible bee_moves from cell.

        A bee can move to a adjacent target position only if:
        - target position is free
        - and there is a piece adjacent to both the bee and that position
        - and there is a free cell that is adjacent to both the bee and the
          target position.
        """
        available_moves = []
        surroundings = self.board.get_surrounding(cell)
        for i in range(6):
            target = surroundings[i-1]
            # is the target cell free?
            if not self._is_cell_free(target):
                continue
            # does it have an adjacent free and an adjancent occupied cell that
            # is also adjacent to the starting cell?
            if (self._is_cell_free(surroundings[i])!= self._is_cell_free(surroundings[i-2])):
                available_moves.append(target)

        return available_moves


    def _piece_set(self, color):
        """
        Return a full set of hive pieces
        """
        pieceSet = {}
        for i in range(3):
            ant = HivePiece(color, 'A', i+1)
            pieceSet[str(ant)] = ant
            grasshopper = HivePiece(color, 'G', i+1)
            pieceSet[str(grasshopper)] = grasshopper
        for i in range(2):
            spider = HivePiece(color, 'S', i+1)
            pieceSet[str(spider)] = spider
            beetle = HivePiece(color, 'B', i+1)
            pieceSet[str(beetle)] = beetle
        queen = HivePiece(color, 'Q', 1)
        pieceSet[str(queen)] = queen
        return pieceSet
    

# +++               +++
# +++ One Hive rule +++
# +++               +++
    def _one_hive(self, piece):
        
        """
        Check if removing a piece doesn't break the one hive rule.
        Returns False if the hive is broken.
        """
        if self.turn>2:
            originalPos = self.locate(str(piece))
        # if the piece is not in the board then moving it won't break the hive
            if originalPos is None:
                return True
        # if there is another piece in the same cell then the one hive rule
        # won't be broken
            pic = self.piecesInCell[originalPos]
            if len(pic) > 1:
                return True

        # temporarily remove the piece
            del self.piecesInCell[originalPos]

        # Get all pieces that are in contact with the removed one and try to
        # reach all of them from one of them.
            occupied = self._occupied_surroundings(originalPos)

            visited = set()
            toExplore = set([occupied[0]])
            toReach = set(occupied[1:])
            res = False

            while len(toExplore) > 0:
                found = []
                for cell in toExplore:
                    found += self._occupied_surroundings(cell)
                    visited.add(cell)
                toExplore = set(found) - visited
                if toReach.issubset(visited):
                    res = True
                    break

        # restore the removed piece
            self.piecesInCell[originalPos] = pic
            return res

# --- ---

# +++                +++
# +++ Movement rules +++
# +++                +++
    def _valid_ant_move(self, ant, startCell, endCell):
        
        # check if ant has no piece on top blocking the move
        if self.piecesInCell[startCell][-1] != str(ant):
            return False
        # temporarily remove ant
        self.piecesInCell[startCell].remove(str(ant))

        toExplore = set([startCell])
        visited = set([startCell])
        res = False

        while len(toExplore) > 0:
            found = set()
            for c in toExplore:
                found.update(self._bee_moves(c))
            found.difference_update(visited)       #update 'found' by elements in found and not in visited.
            # have we found the endCell?
            if endCell in found:
                res = True
                break

            visited.update(found)
            toExplore = found

        # restore ant to it's original position
        self.piecesInCell[startCell].append(str(ant))
        return res


    def _valid_beetle_move(self, beetle, startCell, endCell):
        # check if beetle has no piece on top blocking the move
        if not self.piecesInCell[startCell][-1] == str(beetle):
            return False
        # temporarily remove beetle
        self.piecesInCell[startCell].remove(str(beetle))

        res = False
        # are we on top of the hive?
        if len(self.piecesInCell[startCell]) > 0:
            res = endCell in self.board.get_surrounding(startCell)
        else:
            res = endCell in (self._bee_moves(startCell) + self._occupied_surroundings(startCell))

        # restore beetle to it's original position
        self.piecesInCell[startCell].append(str(beetle))

        return res


    def _valid_grasshopper_move(self, grasshopper, startCell, endCell):
        # TODO: add function to HexBoard that find cells in a straight line

        # is the move in only one direction?
        (sx, sy) = startCell
        (ex, ey) = endCell
        dx = ex - sx
        dy = ey - sy
        p = sy % 2  # starting from an even or odd line?

        # horizontal jump
        if dy == 0:
            # must jump at least over one piece
            if abs(dx) <= 1:
                return False
        # diagonal jump (dy != 0)
        else:
            # must jump at least over one piece
            if abs(dy) <= 1:
                return False

        moveDir = self.board.get_line_dir(startCell, endCell)
        # must move in a straight line
        if moveDir is None or moveDir == 0:
            return False

        # are all in-between cells occupied?
        c = self.board.get_dir_cell(startCell, moveDir)
        while c != endCell:
            if self._is_cell_free(c):
                return False
            c = self.board.get_dir_cell(c, moveDir)

        # is the endCell free?
        if not self._is_cell_free(endCell):
            return False

        return True


    def _valid_queen_move(self, queen, startCell, endCell):
        return endCell in self._bee_moves(startCell)


    def _valid_spider_move(self, spider, startCell, endCell):
        # check if spider has no piece on top blocking the move
        if not self.piecesInCell[startCell][-1] == str(spider):
            return False
        # temporarily remove spider
        self.piecesInCell[startCell].remove(str(spider))


        visited = set()
        firstStep = set()
        secondStep = set()
        thirdStep = set()

        visited.add(startCell)

        firstStep.update(set(self._bee_moves(startCell)))
        visited.update(firstStep)

        for c in firstStep:
            secondStep.update(set(self._bee_moves(c)))
        secondStep.difference_update(visited)
        visited.update(secondStep)

        for c in secondStep:
            thirdStep.update(set(self._bee_moves(c)))
        thirdStep.difference_update(visited)

        # restore spider to it's original position
        self.piecesInCell[startCell].append(str(spider))

        return endCell in thirdStep
    
    
    
    def valid_ant_moves(self):
        validAntMoves=[]
        ppieces = self.playedPieces
  
        if self.get_active_player()== 'w':
            for c in ppieces:
                if c == 'wA1':
                    validAntMoves.extend( self._valid_ant(HivePiece('w','A','1')))
                if c == 'wA2':
                    validAntMoves.extend( self._valid_ant(HivePiece('w','A','2')))
                if c == 'wA3':
                    validAntMoves.extend( self._valid_ant(HivePiece('w','A','3')))
                    
        elif self.get_active_player()== 'b':
            for c in ppieces:
                if c == 'bA1':
              
                    validAntMoves.extend( self._valid_ant(HivePiece('b','A','1')))
                if c == 'bA2':
                    validAntMoves.extend( self._valid_ant(HivePiece('b','A','2')))
                if c == 'bA3':
                    validAntMoves.extend( self._valid_ant(HivePiece('b','A','3')))
                
                
        return validAntMoves

    def _valid_ant(self,ant):
        startCell = self.playedPieces[str(ant)]['cell']


        unocc= self.unoccupiedCells
  
        vMoves=[]
              
              
        for endCell in unocc:
            
                    
            temp=[]
            if self._validate_move_piece(ant, endCell):
                
                    
                temp.append(startCell)
                temp.append(endCell)
                vMoves.append(temp)

        return vMoves
    
    
    
    
    def valid_beetle_moves(self):
        validBeetleMoves=[]
        ppieces = self.playedPieces
        if self.get_active_player()== 'w':
            for c in ppieces:
                if c == 'wB1':
                    validBeetleMoves.extend( self._valid_beetle(HivePiece('w','B','1')))
                if c == 'wB2':
                    validBeetleMoves.extend( self._valid_beetle(HivePiece('w','B','2')))
        elif self.get_active_player()== 'b':
            for c in ppieces:
                if c == 'bB1':
                    validBeetleMoves.extend( self._valid_beetle(HivePiece('b','B','1')))
                if c == 'bB2':
                    validBeetleMoves.extend( self._valid_beetle(HivePiece('b','B','2')))
                
        return validBeetleMoves

    def _valid_beetle(self,beetle):
        
        startCell = self.playedPieces[str(beetle)]['cell']


        unocc= self.unoccupiedCells
   
        vMoves=[]
              
              
        for endCell in unocc:
            
                    
            temp=[]
            if self._validate_move_piece(beetle, endCell):
                
                    
                temp.append(startCell)
                temp.append(endCell)
                vMoves.append(temp)

        return vMoves  
    
    
    def valid_grasshopper_moves(self):
        validGrasshopperMoves=[]
        valid_grass=[]
        ppieces = self.playedPieces
        if self.get_active_player()== 'w':
            for c in ppieces:
                if c == 'wG1':
                    valid_grass.extend( self._valid_grasshopper(HivePiece('w','G','1')))
                if c == 'wG2':
                    valid_grass.extend( self._valid_grasshopper(HivePiece('w','G','2')))
                if c == 'wG3':
                    valid_grass.extend( self._valid_grasshopper(HivePiece('w','G','3')))
        elif self.get_active_player()== 'b':
            for c in ppieces:
                if c == 'bG1':
                    valid_grass.extend( self._valid_grasshopper(HivePiece('b','G','1')))
                if c == 'bG2':
                    valid_grass.extend( self._valid_grasshopper(HivePiece('b','G','2')))
                if c == 'bG3':
                    valid_grass.extend( self._valid_grasshopper(HivePiece('b','G','3')))
        validGrasshopperMoves.extend(valid_grass)
        return validGrasshopperMoves

    def _valid_grasshopper(self,grasshopper):
        
        startCell = self.playedPieces[str(grasshopper)]['cell']
         
        unocc=[]
        unocc= self.unoccupiedCells
   
        vMoves=[]
              
              
        for endCell in unocc:
            
                    
            temp=[]
            if self._validate_move_piece(grasshopper, endCell):
                
                    
                temp.append(startCell)
                temp.append(endCell)
                vMoves.append(temp)

        return vMoves
    
    
    def valid_spider_moves(self):
        validSpiderMoves=[]
        ppieces = self.playedPieces
        if self.get_active_player()== 'w':
            for c in ppieces:
                if c == 'wS1':
                    validSpiderMoves.extend( self._valid_spider(HivePiece('w','S','1')))
                if c == 'wS2':
                    validSpiderMoves.extend( self._valid_spider(HivePiece('w','S','2')))
                if c == 'wS3':
                    validSpiderMoves.extend( self._valid_spider(HivePiece('w','S','3')))
        elif self.get_active_player()== 'b':
            for c in ppieces:
                if c == 'bS1':
                    validSpiderMoves.extend( self._valid_spider(HivePiece('b','S','1')))
                if c == 'bS2':
                    validSpiderMoves.extend( self._valid_spider(HivePiece('b','S','2')))
                if c == 'bS3':
                    validSpiderMoves.extend( self._valid_spider(HivePiece('b','S','3')))
                
        return validSpiderMoves

    def _valid_spider(self,spider):
        
        startCell = self.playedPieces[str(spider)]['cell']


        unocc= self.unoccupiedCells
    
        vMoves=[]
              
              
        for endCell in unocc:
            
                    
            temp=[]
            if self._validate_move_piece(spider, endCell):
                
                    
                temp.append(startCell)
                temp.append(endCell)
                vMoves.append(temp)

        return vMoves
    

    def valid_queen_moves(self):
        validQueenMoves=[]
        ppieces = self.playedPieces
        if self.get_active_player()== 'w':
            for c in ppieces:
                if c == 'wQ1':
                    validQueenMoves.extend( self._valid_queen(HivePiece('w','Q','1')))
        elif self.get_active_player()== 'b':
            for c in ppieces:
                if c == 'bQ1':
                    validQueenMoves.extend( self._valid_queen(HivePiece('b','Q','1')))
        return validQueenMoves

    def _valid_queen(self,queen):
        
        startCell = self.playedPieces[str(queen)]['cell']


        unocc= self.unoccupiedCells
   
        vMoves=[]
              
              
        for endCell in unocc:
            
                    
            temp=[]
            if self._validate_move_piece(queen, endCell):
                
                    
                temp=[]
                temp.append(startCell)
                temp.append(endCell)
                vMoves.append(temp)

        return vMoves
    
    def place_queen(self):
        validQueenMoves=[]
        if self.get_active_player()== 'w':
            for q in self.unplayedPieces['w']:
                if q=='wQ1':
                    validQueenMoves.extend(self._valid_place(HivePiece('w','Q','1')))
                    break
        elif self.get_active_player()== 'b':
            for q in self.unplayedPieces['b']:
                if q=='bQ1':
                    validQueenMoves.extend(self._valid_place(HivePiece('b','Q','1')))
                    break
        return validQueenMoves    
        
    def valid_place_pieces(self):
        validPlacePieces=[]
        ant=[]
        beetle=[]
        grasshopper=[]
        spider=[]
        queen=[]
        antb=[]
        beetleb=[]
        grasshopperb=[]
        spiderb=[]
        queenb=[]
        
        if self.get_active_player()== 'w':

            for a in self.unplayedPieces['w']:
                if a=='wA1':
                    ant.append(a)
                elif a=='wA2':
                    ant.append(a)
                elif a=='wA3':
                    ant.append(a)

            for b in self.unplayedPieces['w']:
                if b=='wB1':
                    beetle.append(b)
                elif b=='wB2':
                    beetle.append(b)
           

            for g in self.unplayedPieces['w']:
                if g=='wG1':
                    grasshopper.append(g)
                elif g=='wG2':
                    grasshopper.append(g)
                elif g=='wG3':
                    grasshopper.append(g)

            for s in self.unplayedPieces['w']:
                if s=='wS1':
                    spider.append(s)
                elif s=='wS2':
                    spider.append(s)
                elif s=='wS3':
                    spider.append(s)
            for q in self.unplayedPieces['w']:
                if q=='wQ1':
                    queen.append(q)
             
            
                for c in ant:
                    if c == 'wA1':
                        validPlacePieces.extend(self._valid_place(HivePiece('w','A','1')))
                        break
            
                    elif c == 'wA2':
                        validPlacePieces.extend(self._valid_place(HivePiece('w','A','2')))
                        break
            
                    elif c == 'wA3':
                        validPlacePieces.extend(self._valid_place(HivePiece('w','A','3')))
                        break
                for c in beetle:   
                    if c == 'wB1':
                        validPlacePieces.extend(self._valid_place(HivePiece('w','B','1')))
                        break
                    elif c == 'wB2':
                        validPlacePieces.extend(self._valid_place(HivePiece('w','B','2')))
                        break
                for c in  grasshopper:
                    if c == 'wG1':
                        validPlacePieces.extend(self._valid_place(HivePiece('w','G','1')))
                        break
                    elif c == 'wG2':
                        validPlacePieces.extend(self._valid_place(HivePiece('w','G','2')))
                        break
                    elif c == 'wG3':
                        validPlacePieces.extend(self._valid_place(HivePiece('w','G','3')))
                        break
                for c in spider:
                    if c == 'wS1':
                        validPlacePieces.extend(self._valid_place(HivePiece('w','S','1')))
                        break
                    elif c == 'wS2':
                        validPlacePieces.extend(self._valid_place(HivePiece('w','S','2')))
                        break
                    elif c == 'wS3':
                        validPlacePieces.append(self._valid_place(HivePiece('w','S','3')))
                        break
                for c in queen:
                    if self.turn>=3:
                        if c == 'wQ1':
                            validPlacePieces.extend(self._valid_place(HivePiece('w','Q','1')))
                            break


            
        elif self.get_active_player()== 'b':
                
                for a in self.unplayedPieces['b']:
                    if a=='bA1':
                        antb.append(a)
                    elif a=='bA2':
                        antb.append(a)
                    elif a=='bA3':
                        antb.append(a)

                for b in self.unplayedPieces['b']:
                    if b=='bB1':
                        beetleb.append(b)
                    elif b=='bB2':
                        beetleb.append(b)
            

                for g in self.unplayedPieces['b']:
                    if g=='bG1':
                        grasshopperb.append(g)
                    elif g=='bG2':
                        grasshopperb.append(g)
                    elif g=='bG3':
                        grasshopperb.append(g)

                for s in self.unplayedPieces['b']:
                    if s=='bS1':
                        spiderb.append(s)
                    elif s=='bS2':
                        spiderb.append(s)
                    elif s=='bS3':
                        spiderb.append(s)
                for q in self.unplayedPieces['b']:
                    if q=='bQ1':
                        queenb.append(q)
                
            
                    for c in antb:
                        if c == 'bA1':
                            validPlacePieces.extend(self._valid_place(HivePiece('b','A','1')))
                            break
                        elif c == 'bA2':
                            validPlacePieces.extend(self._valid_place(HivePiece('b','A','2')))
                            break
                        elif c == 'bA3':
                            validPlacePieces.extend(self._valid_place(HivePiece('b','A','3')))
                            break
                    for c in beetleb:
                        if c == 'bB1':
                            validPlacePieces.extend(self._valid_place(HivePiece('b','B','1')))
                            break
                        elif c == 'bB2':
                            validPlacePieces.extend(self._valid_place(HivePiece('b','B','2')))
                            break
                    for c in grasshopperb:
                        if c == 'bG1':
                            validPlacePieces.extend(self._valid_place(HivePiece('b','G','1')))
                            break
                        elif c == 'bG2':
                            validPlacePieces.extend(self._valid_place(HivePiece('b','G','2')))
                            break
                        elif c == 'bG3':
                            validPlacePieces.extend(self._valid_place(HivePiece('b','G','3')))
                            break
                    for c in spiderb:
                        if c == 'bS1':
                            validPlacePieces.extend(self._valid_place(HivePiece('b','S','1')))
                            break
                        elif c == 'bS2':
                            validPlacePieces.extend(self._valid_place(HivePiece('b','S','2')))
                            break
                        elif c == 'bS3':
                            validPlacePieces.extend(self._valid_place(HivePiece('b','S','3')))
                            break
                    for c in queenb:
                        if self.turn>=3:
                            if c == 'bQ1':
                                validPlacePieces.extend(self._valid_place(HivePiece('b','Q','1')))
                                break
        self.remove(validPlacePieces)
        return validPlacePieces
    def _valid_place(self,piece):
        unocc=self.unoccupiedCells
     
        vMoves=[]
        for endCell in unocc:
            temp=[]
            if self._validate_place_piece(piece,endCell):
                temp.append(piece)
                temp.append(endCell)
                vMoves.append(temp)
        return vMoves
    
    def _unoccupied_cells(self):
        unocc=[]
        pic=self.piecesInCell.keys()
        temp=[]

        for c in pic:
            temp.extend(self._unoccupied_surroundings(c))
            unocc= self.remove(temp)
        return unocc
    
    def possible_moves(self):
        self.unoccupiedCells=self._unoccupied_cells()

        possibleMoves=[]
        if self.turn==1:
            self.unoccupiedCells=[(0,0)]
    
        if (self.turn<=6 and 'wQ1' in self.unplayedPieces['w']) or (self.turn<=6 and 'bQ1' in self.unplayedPieces['b']):
            p=self.valid_place_pieces()
            possibleMoves.extend(p)
            return possibleMoves
            
            
        elif self.turn==7 and 'wQ1' not in self.playedPieces:

            for c in self.unplayedPieces['w']:
                
                if c== 'wQ1':
                    wqueen=self.place_queen()
                    possibleMoves.extend(wqueen)
                    
        elif self.turn==8 and 'bQ1' not in self.playedPieces :
            for c in self.unplayedPieces['b']:
                if c== 'bQ1':
                    bqueen= self.place_queen()
                    possibleMoves.extend(bqueen)
                    
        else:
            pl=self.valid_place_pieces()
            possibleMoves.extend(pl)
            an=self.valid_ant_moves()
 
            possibleMoves.extend(an)
            be=self.valid_beetle_moves()
            possibleMoves.extend(be)
            gr=self.valid_grasshopper_moves()
            possibleMoves.extend(gr)
            q=self.valid_queen_moves()
            possibleMoves.extend(q)

            sp=self.valid_spider_moves()
            possibleMoves.extend(sp)
            
        
            self.remove(possibleMoves)
        return possibleMoves
    
    
    def alignBoard(self):
        playedp={}
        pic={}
                
        pp=self.playedPieces
        qx,qy=self.locate('bQ1')

        for k,v in pp.items():
            x,y = self.locate(k)
            if qy%2==1:
                if y%2==1:
                    end=(x-qx,y-qy)
                else:
                    end=(x-(qx+1),y-qy)
            else:
                end=(x-qx,y-qy)
                
            playedp[str(HivePiece(k[0],k[1],k[2]))]={'piece':HivePiece(k[0],k[1],k[2]),'cell':end}
            pic[end]=[str(HivePiece(k[0],k[1],k[2]))]
       
          
        uc=self.unoccupiedCells
        temp=[]

        for a in uc:
            q,w=a
            cell=q-qx,w-qy
            temp.append(cell)
            uc=temp
        
        self.unoccupiedCells=uc
        if self.turn<10:
            refox=self.board.ref0x+qx+4
            refoy=self.board.ref0y+qy+4
            self.board.resize((-refox,-refoy))
            self.board.resize((refox,refoy))
            self.board.ref0x=refox            
            self.board.ref0y=refoy
        self.playedPieces=playedp

        self.piecesInCell=pic
 
        queen=[]
        num_rot=0
        b=None
        while num_rot!=6:
            aaa=self._rotate()
            queen.append(self.locate('wQ1'))
            num_rot+=1
        queen.sort(key=lambda x:x[1])
        if (queen[1][1])==queen[0][1]:
            c=[queen[0],queen[1]]
            b=min(c, key = lambda t: t[0])
        else:
            b=queen[0]
        wq=self.locate('wQ1')
        while wq!=b:
            aa=self._rotate()
            wq=self.locate('wQ1')

            wx,wy=self.locate('wQ1')
        self.unoccupiedCells=self._unoccupied_cells()    

        return True


    
  
    def _unoccupied_cells(self):
        unocc=[]
        pic=self.piecesInCell.keys()
        temp=[]
        for c in pic:
            
            temp.extend(self._unoccupied_surroundings(c))
         
            unocc= self.remove(temp)
          
        return unocc
    
    
    def old_surrounidings(self,pc):
        cell=self.locate(pc)
        
        wt=self.board.get_w_xy(cell)
        wCell=wt
        wpt=self.get_pieces(wCell)
        wPc=wpt
        et=self.board.get_e_xy(cell)
        eCell=et
        ept=self.get_pieces(eCell)
        ePc=ept
        nwt=self.board.get_nw_xy(cell)
        nwCell=nwt
        nwt=self.get_pieces(nwCell)
        nwPc=nwt
        net=self.board.get_ne_xy(cell)
        neCell=net
        nept=self.get_pieces(neCell)
        nePc=nept
        swt=self.board.get_sw_xy(cell)
        swCell=swt
        swpt=self.get_pieces(swCell)
        swPc=swpt
        sett=self.board.get_se_xy(cell)
        seCell=sett
        sept=self.get_pieces(seCell)
        sePc=sept
        
      
        temp=[]
        temp=[(wPc,wCell) , (nwPc,nwCell) , (nePc,neCell) , (ePc,eCell) , (sePc,seCell) , (swPc,swCell)]
        return temp
    
    
    def _rotate(self):
        oldSur={}
        sur=[]
        pp=self.playedPieces.keys()
        for c in pp:
            oldSur[str(c)]=self.old_surrounidings(c)
        playedpcs=dict(self.playedPieces)   
        ppieces=dict(self.playedPieces)
        pic={}
        newloc={}
        visited=['bQ1']
        playedkeys=list(playedpcs.keys())

        i=playedkeys.index('bQ1')
        q= playedkeys.pop(i)
        (wwPc,wwCell) , (nnwPc,nnwCell) , (nnePc,nneCell) , (eePc,eeCell) , (ssePc,sseCell) , (sswPc,sswCell)=oldSur.get(q)
        wPc=wwPc
        wCell=wwCell
        nwPc=nnwPc
        nwCell=nnwCell
        nePc=nnePc
        neCell=nneCell
        ePc=eePc
        eCell=eeCell
        sePc=ssePc
        seCell=sseCell
        swPc=sswPc
        swCell=sswCell
        if wPc!=[]:
            wp=str(wPc[0])
            ppieces[str(HivePiece(wp[0],wp[1],wp[2]))]={'piece':HivePiece(wp[0],wp[1],wp[2]), 'cell':nwCell}
            visited.append(str(wPc[0]))
            sur.append(str(wPc[0]))
            newloc[str(wPc[0])]=nwCell
        if nwPc!=[]:
            nw=str(nwPc[0])
            ppieces[str(HivePiece(nw[0],nw[1],nw[2]))]={'piece':HivePiece(nw[0],nw[1],nw[2]), 'cell':neCell}
            visited.append(str(nwPc[0]))
            sur.append(str(nwPc[0]))
            newloc[str(nwPc[0])]=neCell
        if nePc!=[]:
            ne=str(nePc[0])
            ppieces[str(HivePiece(ne[0],ne[1],ne[2]))]={'piece':HivePiece(ne[0],ne[1],ne[2]), 'cell':eCell}
            visited.append(str(nePc[0]))
            sur.append(str(nePc[0]))
            newloc[str(nePc[0])]=eCell
        if ePc!=[]:
            e=str(ePc[0])
            ppieces[str(HivePiece(e[0],e[1],e[2]))]={'piece':HivePiece(e[0],e[1],e[2]), 'cell':seCell}
            visited.append(str(ePc[0]))
            sur.append(str(ePc[0]))
            newloc[str(ePc[0])]=seCell
        if sePc!=[]:
            se=str(sePc[0])
            ppieces[str(HivePiece(se[0],se[1],se[2]))]={'piece':HivePiece(se[0],se[1],se[2]), 'cell':swCell}
            visited.append(str(sePc[0]))
            sur.append(str(sePc[0]))
            newloc[str(sePc[0])]=swCell
        if swPc!=[]:
            sw=str(swPc[0])
            ppieces[str(HivePiece(sw[0],sw[1],sw[2]))]={'piece':HivePiece(sw[0],sw[1],sw[2]), 'cell':wCell}
            visited.append(str(swPc[0]))
            sur.append(str(swPc[0]))
            newloc[str(swPc[0])]=wCell
    
        length=len(self.playedPieces)
        while len(visited)<length:

            pc=sur.pop()
     
            (ww_pc,ww_cell) , (nnw_pc,nnw_cell) , (nne_pc,nne_cell) , (ee_pc,ee_cell) , (sse_pc,sse_cell) , (ssw_pc,ssw_cell)=oldSur.get(pc)
            w_pc=list(ww_pc)
            w_cell=ww_cell
            nw_pc=list(nnw_pc)
            nw_cell=nnw_cell
            ne_pc=list(nne_pc)
            ne_cell=nne_cell
            e_pc=list(ee_pc)
            e_cell=ee_cell
            se_pc=list(sse_pc)
            se_cell=sse_cell
            sw_pc=list(ssw_pc)
            sw_cell=ssw_cell
            newLoc=newloc.get(pc)
            new_w,new_nw,new_ne,new_e,new_se,new_sw= self.board.get_surrounding(newLoc)
     
            if w_pc!=[]:
                if str(w_pc[0]) not in  visited:
                    W=str(w_pc[0])
                    ppieces[str(HivePiece(W[0],W[1],W[2]))]={'piece':HivePiece(W[0],W[1],W[2]), 'cell':new_nw}
                    visited.append(str(w_pc[0]))
                    sur.append(str(w_pc[0]))
                    newloc[str(w_pc[0])]=new_nw
            
            if nw_pc!=[]:
                if str(nw_pc[0]) not in visited:
                    NW=str(nw_pc[0])
                    ppieces[str(HivePiece(NW[0],NW[1],NW[2]))]={'piece':HivePiece(NW[0],NW[1],NW[2]), 'cell':new_ne}
                    visited.append(str(nw_pc[0]))
                    sur.append(str(nw_pc[0]))
                    newloc[str(nw_pc[0])]=new_ne
            
            if ne_pc!=[]:
                if str(ne_pc[0]) not in  visited:
                    NE=str(ne_pc[0])
                    ppieces[str(HivePiece(NE[0],NE[1],NE[2]))]={'piece':HivePiece(NE[0],NE[1],NE[2]), 'cell':new_e}
                    visited.append(str(ne_pc[0]))
                    sur.append(str(ne_pc[0]))
                    newloc[str(ne_pc[0])]=new_e
            
            if e_pc!=[]:
                if str(e_pc[0]) not in visited:
                    E=str(e_pc[0])
                    ppieces[str(HivePiece(E[0],E[1],E[2]))]={'piece':HivePiece(E[0],E[1],E[2]), 'cell':new_se}
                    visited.append(str(e_pc[0]))
                    sur.append(str(e_pc[0]))
                    newloc[str(e_pc[0])]=new_se
            
            if se_pc!=[]:
                if str(se_pc[0]) not in visited:
                    SE=str(se_pc[0])
                    ppieces[str(HivePiece(SE[0],SE[1],SE[2]))]={'piece':HivePiece(SE[0],SE[1],SE[2]), 'cell':new_sw}
                    visited.append(str(se_pc[0]))
                    sur.append(str(se_pc[0]))
                    newloc[str(se_pc[0])]=new_sw
            
            if sw_pc!=[]:
                if str(sw_pc[0]) not in visited:
                    SW=str(sw_pc[0])
                    ppieces[str(HivePiece(SW[0],SW[1],SW[2]))]={'piece':HivePiece(SW[0],SW[1],SW[2]), 'cell':new_w}
                    visited.append(str(sw_pc[0]))
                    sur.append(str(sw_pc[0]))
                    newloc[str(sw_pc[0])]=new_w
            else:
                pass

        pi=[]
        temp=[]
        for k,v in ppieces.items():
            pi.append(v)
        for dic in pi:
            piece,cell=dic['piece'],dic['cell']
            temp.append((piece,cell))
            
        for piece, cell in temp:
            pic[cell]=[str(piece)]
            
            
        self.playedPieces=ppieces
        self.piecesInCell=pic
        return ppieces , pic 

