import numpy as np
import random as rnd
import time
import math
import copy
import pickle



class Tree:
    def __init__(self,state):
        self.state= state
        self.parent= None
        self.children= None
        self.visitCount=0
        self.reward=0
        

class Player:
    def __init__(self):
        try:
            self.transposition_table = pickle.load( open( "transpositionTablePureMcts.p", "rb" ) )
        except:
            self.transposition_table={}
        
        
    def start(self, player):
        
        self.player = player
        
                
                
    def next_move(self, hive):
   
        MCTS=self.mcts(copy.deepcopy(hive), timeLimit=1000)
 
        
        return MCTS
    
    
    
    
    def mcts(self, hive , timeLimit=None ):
        reward=[]
        root=Tree(hive)
        self.timeLimit=timeLimit
        timeLimit = time.time() + self.timeLimit / 1000
        while time.time() < timeLimit:
            child_nodes=self.childNodes(root)
            if any(child_nodes):
                 
                selectNode=rnd.choice(child_nodes)
                selectNode_key= self.hash_key(selectNode)
                if selectNode_key in self.transposition_table.keys():
                    Nod,Rew,Visit=self.transposition_table[selectNode_key]
                    Nod.reward+=Rew
                    Nod.visitCount+=Visit
                    rew=Nod,Nod.reward,Nod.visitCount
                else:
                    if time.time() < timeLimit:
                        N,R=self.rollout(selectNode)
                        rew=N,R,N.visitCount

                        self.transposition_table[self.hash_key(selectNode)]=rew
                reward.append(rew)
        rewlist=[]
        for x,y,z in reward:
            rewlist.append(y)
            
        if rewlist!=[]:
            maxrew= max(rewlist)
        else:
            return None

        for c in reward:
            if c[1]==maxrew:
                bestNode=c[0]
                
        for b in child_nodes:
            if b[1]==bestNode:
                if bestNode.state.isTerminal():
                    pickle.dump( self.transposition_table, open( "transpositionTablePureMcts.p", "wb" ) )
                
                return b[0]
                  
       
             
    def rollout(self,actionNodetup):
        action =actionNodetup[0]
        nod= actionNodetup[1]
        nod.visitCount +=1

        state=copy.deepcopy(nod.state)
        while state.isTerminal()==False:
                         
            possMove=state.possible_moves()
            if len(possMove)>=1:
                move=rnd.choice(possMove)
                state.action('play',move)
            
        
            else:
                state.turn += 1
                state.activePlayer ^= 1  


        if nod.state.activePlayer==state.activePlayer:
            nod.reward +=1
        nodrew=(nod,nod.reward)

        return nodrew
            
        
         
    def childNodes(self,node):
        moves=[]
        possibleMoves=[]
        children=[]
        moves=node.state.possible_moves()
        possibleMoves=self.remove(moves)
        cnode=[]
        i=0
        for move in possibleMoves:
            anode=copy.deepcopy(node)
            cnode.append(anode)
            c=cnode[i].state.action('play',move)
     
            children.append((move,cnode[i]))
            i=i+1
        return children
    
    
    
    def remove(self,duplicate=[(0,0)]):
        """
        Returns the list after removing duplicate elements from the list(duplicate)
        """
        final_list = [] 
        for num in duplicate:
      
            if num not in final_list: 
                final_list.append(num) 
        return final_list 
    
    
    def hash_key(self,node):
        
        
        pp=[]
        temp=[]
        for k,v in node[1].state.playedPieces.items():
            temp.append(v)
            
        for c in temp:
            pp.append((c['piece'],c['cell']))
        
        pp.sort(key=lambda x:str(x[0]))

        key=0
        for (x,y) in pp:
            xx,yy=y
            if xx<0:
                key=key+(hash(x)+(-23*hash(xx))+hash(yy))
            else:
                key=key+(hash(x)+(23*hash(xx)+hash(yy)))
               
        if node[1].state.activePlayer==0:
            player_key=key*2
        else:
            player_key=key*2+1
        return player_key
             
             
         
        